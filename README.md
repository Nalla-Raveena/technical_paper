# SERVICE-ORIENTED ARCHITECTURE




Service-Oriented Architecture (SOA) is a style of software design where services are provided to the other components by application 
components.


## Let's dive deep into it.



* The entire software is not built as one but the different features and different services are broken down into smaller components.
   

*   Let's say we have a software application that provides six features, each of these six features are delivered by six different services, each of these services will have multiple tasks inside them and then the smaller tasks are delivered as one particular feature.


## Let's take an example to understand it better.


* Service-oriented architecture is like an orchestra where each artist is performing with his/her instrument while the music director guides them all.
____

## Characteristics of service-oriented architecture
___

*  Business value
*  Intrinsic inter-operability
*  Shared services
*  Flexibility
*  Evolutionary refinement

___

## There are two major roles within Service-oriented Architecture: 
___
### 1. Service consumer:
* If we have a software call it a service consumer and it wants to talk to another software which is service provider, so the consumer software is going to send a request to provider software.

* The consumer software needs to know how to call these services for example what arguments or parameters the service is expecting also it needs to know what kind of response this service would be sending back to the consumer.

### 2. Service provider:
* The service provider is going to respond with the service response when the request is received from the consumer software.

___
## Advantages of SOA:
___ 

* ###  Service reusability: 

    In SOA, applications are made from existing services. thus, services can be reused to make many applications.

* ### Easy maintenance: 
  One service doesn't depend on other services, services are independent of each other so if we change anything or modify anything within one service won't affect another service.
* ### Platform independent:

  Allows making a complex application by combining services picked from different sources, independent of the platform.

* ### Availability: 
  SOA facilities are easily available to anyone on request.

* ### Reliability:
  More reliable because it maintains small services rather than a huge code, which makes it easy to debug.

* ### Scalability:
  Services can run on different servers within an environment, this increases scalability.

___
##  Disadvantages of SOA:  
___

* ### High cost:
  SOA is costly in terms of human resources, development, and technology.

* ### High bandwidth server:
   It involves a high-speed server with a lot of data bandwidth to run a web service.
___
## Examples of Service-oriented architecture: 
___  
* A website using payment service like PayPal integration
* An e-commerce website using TCS service (or any other service) for physical shipment of products
* Any hotel getting food prepared through any outer service provider
* Any big organization that get extra services from providers to run their business is included in SOA
* SOA infrastructure is used by many armies and air forces to deploy situational awareness systems.
* SOA is used to improve healthcare delivery.
* Nowadays many apps are games and they use inbuilt functions to run. For example, an app might need GPS so it uses the inbuilt GPS functions of the device. This is SOA in mobile solutions.
* SOA helps maintain museums a virtualized storage pool for their information and content.




## SOA reference links:

* [SOA article](https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec#:~:text=Service%2DOriented%20Architecture%20(SOA),of%20vendors%20and%20other%20technologies.)

* [SOA YOUTUBE VIDEO](https://youtu.be/L1tM0tMJdzY)





    
         


